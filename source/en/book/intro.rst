.. _intro-en:

*****************************
Introduction
*****************************

============================
Book topic
============================

This is the draft of the book about software distribution projects in general:

* why such a form of development has appeared;
* typical design and architecture of distributions;
* challenges before distribution developers and their possible solutions.

The most well-known distribution projects are software for
open-source general purpose operating systems from POSIX family, like Linux, BSD or Illumos.
These projects are main coverage topics, but the story is not limited to them.
There are interesting distributions around programming languages
(perl, python, Latex, etc.) with similar quirks.

============================
Mission
============================

Lots of distro developing projects.
Each solving similar tasks, each has their own technical approach, development stack and knowledge base.
Even though all these can be done in the open and can be well documented,
state of things still does not allow extensive interchange.
Solutions are project specific and hardly reusable. At best, they are applicable in the project of relative architecture.

Development for the POSIX platforms has diverged and became specialised for each of them,
even when low-level technical foundations are similar for many.
For example, adding program to OpenBSD infrastructure is highly unlike doing the same for FreeBSD.
As a consequence, application developers support limited set of platforms and leave the rest adrift.
They do not wish to learn and maintain integration mechanisms for every single distro in the world.
That does no good to open source ecosystem. That means more work for platform maintainers, potentially avoidable work.

The book aims to describe software distribution problems and possible approaches in project-agnostic way.
Thus they can be easily understood and adopted by anybody.
Generalised knowledge would allow standalone software authors to cope with multiple POSIX platforms,
effectively integrate and maintain their product upon that diverse field without superstress.
Distro devs can find new ideas and approaches for their issues more fast and easy.

Most program examples are demonstrated with build system `any <https://opendistro.org/any/>`_.
`Any <https://opendistro.org/any/>`_ is designed to work for multiple distributive projects and infrastructures.
As such it can be used directly by application developers for their distro-independent work.
Distro devs may observe solutions in straight and simplistic manner,
as such approach is the goal of universal build system.
Version ``any-0.11.*`` is used at the moment of writing.
When possible, another popular frameworks should be demonstrated in examples as well.

============================
Group of interest
============================

* new distro developers;
* developers of new distros;
* developers of open source or POSIX-oriented software;
* users and newcomers into POSIX operating systems.

