.. _cases-en:

*****************************
Cases
*****************************

Production tasks and issues.
Comprehensive analysis of the task.

Possible solutions.
Requests for new approaches and ideas.
