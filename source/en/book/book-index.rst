.. distrobook documentation master file

*********************************
Distrobook
*********************************

This is the draft of the book about software distribution projects in general:

* why such a form of development has appeared;
* typical design and architecture of distributions;
* challenges before distribution developers and their possible solutions.

The most well-known distribution projects are software for
open-source general purpose operating systems from POSIX family, like Linux, BSD or Illumos.
These projects are main coverage topics, but the story is not limited to them.
There are interesting distributions around programming languages
(perl, python, Latex, etc.) with similar quirks.

Current available version is in russian.

..
    .. toctree::
        :maxdepth: 1
        :caption: Select language
    
        Russian (main one) <ru/index>
        English <en/index>
